CHANGELOG
=========

## 'v1.0' (2023-11-08)
- Optimized using mapgen_rivers data
- Module mapgen_rivers added to dependencies
- Add settingtypes.txt file

## 'v1.0 Beta' (2023-10-13)
- Very slow but working generic logic
