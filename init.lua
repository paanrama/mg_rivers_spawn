-- Mapgen Rivers Spawn
-- Inspired on random_spawn mod by Nathan Salapat and minetest_mod_spawnrand by minenux
mg_rivers_spawn = {}

-- One of the following: "none", "error", "warning", "action", "info", or "verbose"
local debug_log_level = "verbose"
-- active/inactive dev chat tools
local dev_mode = false
-- elevation delta around dem terrain height to emerge
local dem_delta = 10

-- compatibility with sethome module
mg_rivers_spawn.sethome_installed = minetest.get_modpath("sethome")

local mapgen_limit = tonumber(minetest.settings:get("mapgen_limit")) or 30000
mg_rivers_spawn.radius_max = tonumber(minetest.settings:get("mg_rivers_spawn_radius_max")) or 15000
if mg_rivers_spawn.radius_max > mapgen_limit then
	mg_rivers_spawn.radius_max = mapgen_limit
end

mg_rivers_spawn.radius_inc = tonumber(minetest.settings:get("mg_rivers_spawn_radius_inc")) or 1000
local max_cycles = math.ceil(mg_rivers_spawn.radius_max / mg_rivers_spawn.radius_inc)
mg_rivers_spawn.spawn_count_per_block = tonumber(minetest.settings:get("mg_rivers_spawn_spawn_count_per_block")) or 5
mg_rivers_spawn.min_spawn_elevation = tonumber(minetest.settings:get("mg_rivers_spawn_min_spawn_elevation")) or 1
mg_rivers_spawn.max_spawn_elevation = tonumber(minetest.settings:get("mg_rivers_spawn_max_spawn_elevation")) or 100
mg_rivers_spawn.emerge_radius = tonumber(minetest.settings:get("mg_rivers_spawn_emerge_radius")) or 30
mg_rivers_spawn.emerge_area_delay = tonumber(minetest.settings:get("mg_rivers_spawn_emerge_area_delay")) or 0.5
mg_rivers_spawn.new_player_wait_time = tonumber(minetest.settings:get("mg_rivers_spawn_new_player_wait_time")) or 1
mg_rivers_spawn.new_player_max_intents = tonumber(minetest.settings:get("mg_rivers_spawn_new_player_max_intents")) or 10


-- valid spawn nodes: a list of node names or groups
-- the nodes must have a group value of 1 to be included
mg_rivers_spawn.valid_spawn_node_list = {}
local valid_spawn_node_list = minetest.settings:get("mg_rivers_spawn_valid_spawn_node_list") or "group:soil, group:sand"
minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Making valid node list from %s", valid_spawn_node_list))
i = 1
for w in string.gmatch (valid_spawn_node_list, "[%w%_%:]+") do 
	minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Checking %s value", w))
	if string.sub(w, 1, 6) == "group:" then
		for node_name, node_def, value in pairs(minetest.registered_nodes) do
			minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Check group in node %s", node_name))
			-- exists a node with this group
			if minetest.get_item_group(node_name, string.sub(w, 7, string.len(w))) == 1 then
				mg_rivers_spawn.valid_spawn_node_list[i] = w
				minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Group %s added in position %d", w, i))
				i = i + 1
				break
			end
		end
	else
		-- node exists
		if minetest.registered_nodes[w] then
			mg_rivers_spawn.valid_spawn_node_list[i] = w
			minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Node %s added in position %d", w, i))
			i = i + 1
		end
	end
end
-- if valid node list is empty fill it with all nodes
if #mg_rivers_spawn.valid_spawn_node_list == 0 then
    local i = 1
    for node_name, node_def, value in pairs(minetest.registered_nodes) do
        mg_rivers_spawn.valid_spawn_node_list[i] = node_name
    	i = i + 1
    end
end
minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Valid node list is %s", minetest.serialize(mg_rivers_spawn.valid_spawn_node_list)))


-- mapgen rivers data
local blocksize = mapgen_rivers.settings.blocksize
local X = math.floor(mapgen_rivers.settings.map_x_size / blocksize)
local Z = math.floor(mapgen_rivers.settings.map_z_size / blocksize)
local sea_level = mapgen_rivers.settings.sea_level


-- convert (x, y) to mapgen_rivers dem grid index
local function to_grid_dem_index(x, z)
	return z*X+x+1
end


-- check mapgen_rivers grid to see if terrain height is a valid spawn pos
local function get_grid_height(x, z)
	local grid = mapgen_rivers.grid
	local x_grid = math.max(math.floor((x+blocksize*grid.size.x/2)/blocksize), 0)
	local z_grid = math.max(math.floor((z+blocksize*grid.size.y/2)/blocksize), 0)
	if x_grid > (X - 2) then
		x_grid = X - 2
	end
	if z_grid > (Z - 2) then
		z_grid = Z - 2
	end

	local terrain_height = grid.dem[to_grid_dem_index(x_grid, z_grid)]
	local lake_height = grid.lakes[to_grid_dem_index(x_grid, z_grid)]
	minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Position (%d, %d) has terrain height of %d and lake height of %d", x, z, terrain_height, lake_height))
	return terrain_height, lake_height
end


-- get a random (x, z) position
-- each cycle representa a square shape band of wide mg_rivers_spawn.radius_inc that go away of (0, 0)
-- cycle 1 is a circle with center on (0, 0)
-- cycle n is a donut with center on (0, 0) that not include the previous cycle
function mg_rivers_spawn:get_random_pos(cycle)
	local posx, posz, ax, az, bx, bz
	ax = math.random(-mg_rivers_spawn.radius_inc, mg_rivers_spawn.radius_inc)
	az = math.random(-mg_rivers_spawn.radius_inc, mg_rivers_spawn.radius_inc)
	if ax == 0 then
		ax = 1
	end
	if az == 0 then
		az = 1
	end
	local axMag = math.abs(ax)
	local azMag = math.abs(az)
	if axMag > azMag then
		bx = mg_rivers_spawn.radius_inc * (cycle -1)
		bz = math.floor(azMag * bx / axMag)
	else
		bz = mg_rivers_spawn.radius_inc * (cycle - 1)
		bx = math.floor(axMag * bz / azMag)
	end
	-- sign must be adjusted to be on the correct quadrant
	posx = (bx + axMag) * (axMag / ax)
	posz = (bz + azMag) * (azMag / az)
	return posx, posz
end


-- main search function
function mg_rivers_spawn:random_spawn(player, cycle, intent)

	local cycle_max_intent = (math.pow(cycle * 2, 2) - math.pow((cycle - 1) * 2, 2)) * mg_rivers_spawn.spawn_count_per_block
	if intent >= cycle_max_intent then
		cycle = cycle + 1
		intent = 0
	end

	--can not found a valid spawn pos
	if cycle > max_cycles then
		minetest.log("error", string.format("[mg_rivers_spawn] Can not found a valid spawn position..."))
		--minetest.log('action', string.format("[mg_rivers_spawn] Can not found a valid spawn position, set player pos to (0, 0, 0)"))
		--player:set_pos({x = 0, y = 0, z = 0})
	else
		local posx, posz = mg_rivers_spawn:get_random_pos(cycle)
		minetest.log(debug_log_level, string.format("[mg_rivers_spawn] New Random Spawn Position (%d, %d)", posx, posz))
		local terrain_height, lake_height = get_grid_height(posx, posz)
		if (terrain_height > 1) and (terrain_height >= lake_height) then
			mg_rivers_spawn:find_spawn_elevation(player, {x = posx, y = mg_rivers_spawn.min_spawn_elevation, z = posz}, cycle, intent + 1, terrain_height)
		else
			mg_rivers_spawn:random_spawn(player, cycle, intent + 1)
		end
	end
end


-- valid pos verification function
function mg_rivers_spawn:find_spawn_elevation(player, pos, cycle, intent, terrain_height)

	-- only emerge around terrain_height +/- dem_delta
	local posx1, posy1, posz1 =  (pos.x - mg_rivers_spawn.emerge_radius), math.max(mg_rivers_spawn.min_spawn_elevation, (terrain_height - dem_delta)), (pos.z - mg_rivers_spawn.emerge_radius)
	local posx2, posy2, posz2 =  (pos.x + mg_rivers_spawn.emerge_radius), math.min(mg_rivers_spawn.max_spawn_elevation, (terrain_height + dem_delta)), (pos.z + mg_rivers_spawn.emerge_radius)

	minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Searching Spawn Position: intent: %d area: (%d, %d, %d) to (%d, %d, %d)",
		intent, posx1, posy1, posz1, posx2, posy2, posz2))

	-- area not fully loaded, wait and force emerge
	if #minetest.find_nodes_in_area({x = posx1, y = posy1, z = posz1}, {x = posx2, y = posy2, z = posz2}, "ignore") > 0 then
		minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Area from (%d, %d, %d) to (%d, %d, %d) not fully loaded waiting",
			posx1, posy1, posz1, posx2, posy2, posz2))
		minetest.emerge_area({x = posx1, y = posy1, z = posz1}, {x = posx2, y = posy2, z = posz2})
		minetest.after(mg_rivers_spawn.emerge_area_delay,
			(function() mg_rivers_spawn:find_spawn_elevation(player, pos, cycle, intent, terrain_height)
			end))

	else
		local node_list = {}
		node_list = minetest.find_nodes_in_area_under_air({x = posx1, y = posy1, z = posz1}, {x = posx2, y = posy2, z = posz2}, mg_rivers_spawn.valid_spawn_node_list)
		minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Found %d Posible Spawn Positions between (%d, %d, %d) and (%d, %d, %d)", 
			#node_list, posx1, posy1, posz1, posx2, posy2, posz2))

		--node_list is empty, retry in another position
		if #node_list == 0 then
			mg_rivers_spawn:random_spawn(player, cycle, intent)

		--see if node list has a valid position
		else
			local is_valid_pos = false
			local i = 1
			local posAux
			repeat
				posAux = node_list[i]
				minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Validating pos (%d, %d, %d)",
					posAux.x, posAux.y, posAux.z))
				local under_node = minetest.get_node({x = posAux.x, y = posAux.y, z = posAux.z})
				local current_node = minetest.get_node({x = posAux.x, y = posAux.y + 1, z = posAux.z})
				local above_node = minetest.get_node({x = posAux.x, y = posAux.y + 2, z = posAux.z})
				-- check above and under for valid position
				if current_node.name == 'air' and above_node.name == 'air' and under_node.name ~= 'air'
					and minetest.registered_nodes[under_node.name].drawtype ~= 'liquid'
					and minetest.registered_nodes[under_node.name].drawtype ~= 'flowingliquid'
					then

					is_valid_pos = true
				else
					i = i + 1
				end
			until i > #node_list or is_valid_pos

			-- valid pos found, set new player pos
			if is_valid_pos then
				minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Valid Spawn Position found at position (%d, %d, %d) on node %s",
					posAux.x, posAux.y, posAux.z, minetest.get_node(posAux).name))
				player:set_pos({x = posAux.x, y = posAux.y + 1, z = posAux.z})
			else
				mg_rivers_spawn:random_spawn(player, cycle, intent)
			end
		end
	end
end


-- spawn a new player (can wait if player not exists, for example if world is not generated yet)
function mg_rivers_spawn:spawn_new_player(player, intent)
	if player ~= nil then
		mg_rivers_spawn:random_spawn(player, 1, 0)
	else
		if intent > 0  then
			minetest.after(mg_rivers_spawn.new_player_wait_time, (function() mg_rivers_spawn:spawn_new_player(player, intent - 1) end))
		end
	end
end


-- random spam in new player join
minetest.register_on_newplayer(function(player)

	-- initialize lua random seed
	math.randomseed(os.clock())
	mg_rivers_spawn:spawn_new_player(player, mg_rivers_spawn.new_player_max_intents)

end)


-- On player dead try respawn in random position around dead position
-- if there is not valid position around dead position, it do a full
-- random search calling mg_rivers_spawn:random_spawn
minetest.register_on_respawnplayer(function(player)

	if player ~= nil then
		local player_name = player:get_player_name()

		-- check for sethome
		if mg_rivers_spawn.sethome_installed then
			local home_pos = sethome.get(player_name)
			if home_pos then
				sethome.go(player_name)
				return true
			end
		end

		-- not set spawn pos, search a random position
		local player_pos = player:get_pos()
		minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Searching a respawn position for player %s around (%d, %d, %d)",
			player_name, player_pos.x, player_pos.y, player_pos.z))
		mg_rivers_spawn:find_spawn_elevation(player, player_pos, 1, 1, player_pos.y)
		return true

	end

end)


-- DEV TOOLS
if dev_mode then
	minetest.log(debug_log_level, "[mg_rivers_spawn] DEV Mode Active, Registering Spawn Test Chat Command")

	minetest.register_chatcommand("search_spawn_pos", {
		params = "[here] | [<x, y, z>]",
		description = "Search a Valid Spawn Position at the specified position, use here to search at player position or leave empty to do a random search",
		func = function(name, param)
			local player = minetest.get_player_by_name(name)
			local cycle_max_intents = (math.pow(max_cycles * 2, 2) - math.pow((max_cycles - 1) * 2, 2)) * mg_rivers_spawn.spawn_count_per_block

			if player then
				local player_pos = player:get_pos()
				-- search if there is a valid position around player pos
				if param == "here" then
					minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Player %s at (%d, %d, %d) call Spawn Test Chat Command (here)",
						name, player_pos.x, player_pos.y, player_pos.z))
					mg_rivers_spawn:find_spawn_elevation(player, player_pos, max_cycles, cycle_max_intents, player_pos.y)
					return true
				end

				local pos = {}
				pos.x, pos.y, pos.z = string.match(param, "^([%d.-]+)[, ] *([%d.-]+)[, ] *([%d.-]+)$")
				pos.x = tonumber(pos.x)
				pos.y = tonumber(pos.y)
				pos.z = tonumber(pos.z)

				-- no parameters full random search
				if not pos.x or not pos.y or not pos.z then
					minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Player %s at (%d, %d, %d) call Spawn Test Chat Command (Random)",
						name, player_pos.x, player_pos.y, player_pos.z))
					mg_rivers_spawn:random_spawn(player, 1, 0)
					return true
				end

				if (pos.x < -mg_rivers_spawn.radius_max) or (pos.x > mg_rivers_spawn.radius_max) or 
				   (pos.y < -mg_rivers_spawn.radius_max) or (pos.y > mg_rivers_spawn.radius_max) or
				   (pos.z < -mg_rivers_spawn.radius_max) or (pos.z > mg_rivers_spawn.radius_max) then

				   return false, "[mg_rivers_spawn] Position out of range in search_spawn_pos chat command"
				end

				-- search a random valid position around pos indicated by the user
				minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Player %s at (%d, %d, %d) call Spawn Test Chat Command with position (%d, %d, %d)",
					name, player_pos.x, player_pos.y, player_pos.z, pos.x, pos.y, pos.z))
				mg_rivers_spawn:find_spawn_elevation(player, pos, max_cycles, cycle_max_intents, pos.y)
				return true

			end
		end,
	})

	minetest.register_chatcommand("random_pos_test", {
		params = "",
		description = "Test the random pos logic",
		func = function(name, param)
			minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Random Point Logic Test Start"))
			for cycle = 1, max_cycles, 1 do
				local cycle_max_intents = (math.pow(cycle * 2, 2) - math.pow((cycle - 1) * 2, 2)) * mg_rivers_spawn.spawn_count_per_block
				minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Random Point Logic Test Cycle %d, Max Intents %d", cycle, cycle_max_intents))
				for intent = 1, cycle_max_intents, 1 do
					local posx, posz = mg_rivers_spawn:get_random_pos(cycle)
					minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Random Point for Cycle %d, Intent %d: (%d, %d)", cycle, intent, posx, posz))
				end
			end
			minetest.log(debug_log_level, string.format("[mg_rivers_spawn] Random Point Logic Test End"))
			return true

		end,
	})

end
