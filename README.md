# Mapgen Rivers Spawn
Minetest player spawn module for Gaël de Sailly mapgen_rivers map generator.

## Author
Code: Pablo Raffo

Inspired on previous work of:
- Nathan Salapat random_spawn mod
- Minenux minetest_mod_spawnrand mod

Special thanks to Gaël de Silly who develop mapgen_rivers and biomegen minetest modules.

## License
License: GNU LGPLv3.0

## Requirements
mapgen_rivers by Gaël de Sailly

## Installation
This mod should be placed in the mods/ directory of Minetest like any other mod.

## Settings
Settings can be found in Minetest in the `Settings` tab, `All settings` -> `Mods` -> `mapgen_rivers_spawn`.

## Roadmap
A simple list of things to do:
- Add checks for protection, adjacent nodes required, etc.
- Add compatibility with more set home logic (beds or other)
- Add support to a list of near nodes that must be present (under air?)
- Add support to a list of near nodes that must not be present (under air?)

